;----------------------------------------------------------
; Activity: Problem Set: Recursive Functions I
; Date: September 2, 2018.
; Authors:
;          A01377515 Pablo Alejandro Sánchez Tadeo
;          A01377162 Guillermo Perez Trueba
;----------------------------------------------------------

(use 'clojure.test)

;1
(defn my-repeat
  "Takes a number n and any data x as its arguments.
  It returns a list that contains n copies of x."
  [n, x]
  (loop [n n
         x x
         result ()]
    (if (= n 0)
      result
      (recur
        (dec n)
        x
        (cons x result)))))

(deftest test-my-repeat
  (is (= () (my-repeat 0 'x)))
  (is (= '(6 6 6) (my-repeat 3 6)))
  (is (= '((ha ha) (ha ha) (ha ha)) (my-repeat 3 '(ha ha))))
  (is (= '(true true true true true) (my-repeat 5 true))))

;2
(defn invert-pairs
  "Takes as an argument a list of vectors containing two elements each.
  It returns a new list with every vector pair inverted."
  [list]
  (loop [list list
         result ()]
    (if (empty? list)
      (reverse result)
      (recur (rest list) (conj result (into [] (reverse (conj (rest(first list)) (first(first list))))))))))

(deftest test-invert-pairs
  (is (= () (invert-pairs ())))
  (is (= '([1 a][2 a][1 b][2 b]))
      (invert-pairs '([a 1][a 2][b 1][b 2])))
  (is (= '([1 January][2 February][3 March])
         (invert-pairs '([January 1][February 2][March 3])))))

;3
(defn enlist
  "Surrounds in a list every upper-level element
  of the list it takes as input."
  [lst]
  (loop [lst lst
         result []]
    (if (empty? lst)
      result
      (recur
        (rest lst)
        (conj result (cons (first lst) ()))))))

(deftest test-enlist
  (is (= () (enlist ())))
  (is (= '((a) (b) (c)) (enlist '(a b c))))
  (is (= '(((1 2 3)) (4) ((5)) (7) (8))
         (enlist '((1 2 3) 4 (5) 7 8)))))

;4
(defn my-interleave
  "Takes two arguments: the lists a and b. It returns a list containing the first element of a,
  followed by the first element of b, followed by the second element of a, followed by the second element of b,
  and so on. The lists a and b don't have to be of the same size.
  The interleaving of the elements stops when either a or b is exhausted."
  [a b]
  (loop [a a
         b b
         result ()]
    (cond
      (empty? a) (reverse result)
      (empty? b) (reverse result)
      :else (recur (rest a) (rest b)  (cons (first b) (cons (first a) result))))))

(deftest test-my-interleave
  (is (= () (my-interleave () ())))
  (is (= () (my-interleave '(a) ())))
  (is (= () (my-interleave () '(1))))
  (is (= '(a 1 b 2 c 3 d 4 e 5)
         (my-interleave '(a b c d e) '(1 2 3 4 5))))
  (is (= '(a 1 b 2 c 3 d 4)
         (my-interleave '(a b c d e) '(1 2 3 4))))
  (is (= '(a 1 b 2 c 3 d 4)
         (my-interleave '(a b c d) '(1 2 3 4 5))))
  (is (= '(a 1) (my-interleave '(a) '(1 2 3 4 5))))
  (is (= '(a 1) (my-interleave '(a b c d e) '(1)))))

;5
(defn my-flatten
  "Removes all the interior parenthesis of the list
  it takes as input."
  [lst]
  (if (empty? lst)
    lst
    (if (list? (first lst))
      (concat (my-flatten (first lst)) (my-flatten (rest lst)))
      (cons (first lst) (my-flatten (rest lst))))))

(deftest test-my-flatten
  (is (= () (my-flatten ())))
  (is (= '(a b c d e) (my-flatten '((a b) ((c) d (e))))))
  (is (= '(one two three four)
         (my-flatten '(((one) ((two))) () (three (())) four)))))

;6
(defn exchange
  "Takes three arguments: two non-list values x1 and x2, and a list lst.
  It returns a list with the same elements as lst, except that all occurrences of x1 are
  replaced by x2 and vice versa, including any occurrences inside nested lists."
  [x1 x2 lst]

  (loop[x1 x1
        x2 x2
        lst lst
        result ()]
    (if (empty? lst)
      (reverse result)
      (cond
        (= (first lst) x1) (recur x1 x2 (rest lst) (cons x2 result))
        (= (first lst) x2) (recur x1 x2 (rest lst) (cons x1 result))
        :else (recur x1 x2 (rest lst) (cons (first lst) result))))))

(deftest test-exchange
  (is (= () (exchange 'x 'y ())))
  (is (= '(d b c a) (exchange 'a 'd '(a b c d))))
  (is (= '((42) true ((cool (true)) (42))))
      (exchange true 42 '((true) 42 ((cool (42)) (true))))))

;7
(defn insert
  "Takes two arguments: a number n and a list of numbers lst
   in ascending order. It returns a new list with the same
   elements as lst but inserting n in its corresponding place."
  [x lst]
  (if (empty? lst)
    (cons x lst)
    (if (> (first lst) x)
      (cons x lst)
      (cons (first lst) (insert x (rest lst))))))

(deftest test-insert
  (is (= '(14) (insert 14 ())))
  (is (= '(4 5 6 7 8) (insert 4 '(5 6 7 8))))
  (is (= '(1 3 5 6 7 9 16) (insert 5 '(1 3 6 7 9 16))))
  (is (= '(1 5 6 10) (insert 10 '(1 5 6)))))

;8
(defn my-sort
  "Takes an unordered list of numbers as an argument, and returns a new list with the same elements
  but in ascending order. Uses the insert function defined in the previous exercise."
  [list]
  (loop[list list
        result ()]
    (if (empty? list)
     result
        (recur (rest list) (insert (first list) result)))))

(deftest test-my-sort
  (is (= () (my-sort ())))
  (is (= '(0 1 3 3 4 6 7 8 9) (my-sort '(4 3 6 8 3 0 9 1 7))))
  (is (= '(1 2 3 4 5 6) (my-sort '(1 2 3 4 5 6))))
  (is (= '(1 5 5 5 5 5 5) (my-sort '(5 5 5 1 5 5 5)))))

;9
(defn binary
  "Takes an integer n as input (assume that n ≥ 0).
  If n is equal to zero, it returns an empty list.
  If n is greater than zero, it returns a list with
  a sequence of ones and zeros equivalent to the
  binary representation of n."
  [n]
  (cond
    (zero? n) []
    (= (mod n 2) 0) (conj (binary (/ n 2)) 0)
    :else (conj (binary (/ (- n 1) 2)) 1)))

(deftest test-binary
  (is (= () (binary 0)))
  (is (= '(1 1 1 1 0) (binary 30)))
  (is (= '(1 0 1 1 0 0 0 0 0 1 0 0 0 0 1 1) (binary 45123))))

;10
(defn prime-factors
  "Takes an integer n as input (assume that n > 0), and returns a list containing the prime
   factors of n in ascending order. The prime factors are the prime numbers that divide a number exactly"
  [n]
  (loop[n n
        i 2
        result ()]
    (if (<= n 1)
      (reverse result)
      (if (= (rem n i) 0)
        (recur (/ n i) i (cons i result))
        (recur n (inc i) result)))))

(deftest test-prime-factors
  (is (= () (prime-factors 1)))
  (is (= '(2 3) (prime-factors 6)))
  (is (= '(2 2 2 2 2 3) (prime-factors 96)))
  (is (= '(97) (prime-factors 97)))
  (is (= '(2 3 3 37) (prime-factors 666))))

;11
(defn compress
  "Takes a list lst as its argument. If lst contains
  consecutive repeated elements, they should be
  replaced with a single copy of the element."
  [lst]
  (if (empty? lst)
    ()
    (if (not (= (first lst) (first (rest lst))))
      (cons (first lst) (compress (rest lst)))
      (compress (rest lst)))))

(deftest test-compress
  (is (= () (compress ())))
  (is (= '(a b c d) (compress '(a b c d))))
  (is (= '(a b c a d e)
         (compress '(a a a a b c c a a d e e e e))))
  (is (= '(a) (compress '(a a a a a a a a a a)))))

;12
(defn pack
  "Takes a list lst as its argument. If lst contains consecutive repeated elements they should
  be placed in separate sublists"
  [lst]
  (loop [list lst
         sublist ()
         result ()]
    (if (empty? list)
      (reverse result)
      (if (=(first list) (first (rest list)))
        (recur (rest list) (cons (first list) sublist) result)
        (recur (rest list) '() (conj result (cons (first list) sublist)))))))

(deftest test-pack
  (is (= () (pack ())))
  (is (= '((a a a a) (b) (c c) (a a) (d) (e e e e))
         (pack '(a a a a b c c a a d e e e e))))
  (is (= '((1) (2) (3) (4) (5)) (pack '(1 2 3 4 5))))
  (is (= '((9 9 9 9 9 9 9 9 9)) (pack '(9 9 9 9 9 9 9 9 9)))))

;13
(defn encode
  "Takes a list lst as its argument. Consecutive
  duplicates of elements in lst are encoded as
  vectors [n e], where n is the number of duplicates
  of the element e."
  [lst]
  (loop [lst lst
         cont 0
         res []]
    (if (empty? lst)
      res
      (recur
        (rest lst)
        (if (not (= (first lst) (first (rest lst))))
          0
          (+ cont 1))
        (if (not (= (first lst) (first (rest lst))))
          (conj res (conj [] (+ cont 1) (first lst)))
          res)))))


(deftest test-encode
  (is (= () (encode ())))
  (is (= '([4 a] [1 b] [2 c] [2 a] [1 d] [4 e])
         (encode '(a a a a b c c a a d e e e e))))
  (is (= '([1 1] [1 2] [1 3] [1 4] [1 5]) (encode '(1 2 3 4 5))))
  (is (= '([9 9]) (encode '(9 9 9 9 9 9 9 9 9)))))

;14
(defn encode-modified
  "Takes a list lst as its argument. If an element has no duplicates it is simply copied into the result list.
  Only elements with duplicates are converted to [n e] vectors."
  [list]

  (loop [list list
         i 1
         vector ()
         result ()]
    (if (empty? list)
      (reverse result)
      (if (=(first list) (first (rest list)))
        (recur (rest list) (inc i) vector result)
        (cond
          (> i 1) (recur (rest list) 1  [] (conj result (into [] (cons i (cons (first list) vector)))))
          :else (recur (rest list) 1  [] (cons (first list) result)))))))



(deftest test-encode-modified
  (is (= () (encode-modified ())))
  (is (= '([4 a] b [2 c] [2 a] d [4 e])
         (encode-modified '(a a a a b c c a a d e e e e))))
  (is (= '(1 2 3 4 5) (encode-modified '(1 2 3 4 5))))
  (is (= '([9 9]) (encode-modified '(9 9 9 9 9 9 9 9 9)))))

;15
(defn decode
  "Takes as its argument an encoded list lst that has
  the same structure as the resulting list from the
  previous problem. It returns the decoded version of lst."
  [lst]
  (loop [res []
         lst lst]
    (if (empty? lst)
      res
      (recur
        (if (vector? (first lst))
          (into [] (concat res ((fn dec [n a]
                                  (loop [n n
                                         a a
                                         res ()]
                                    (if (zero? n)
                                      res
                                      (recur
                                        (- n 1)
                                        a
                                        (cons a res))))) (first (first lst)) (first (rest (first lst))))))
          (conj res (first lst)))
        (rest lst)))))

(deftest test-decode
  (is (= () (decode ())))
  (is (= '(a a a a b c c a a d e e e e)
         (decode '([4 a] b [2 c] [2 a] d [4 e]))))
  (is (= '(1 2 3 4 5) (decode '(1 2 3 4 5))))
  (is (= '(9 9 9 9 9 9 9 9 9) (decode '([9 9])))))

(run-tests)