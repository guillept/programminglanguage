;----------------------------------------------------------
; Activity: Problem Set: Macros
; Date: November 11, 2018.
; Authors:
;          A01377515 Pablo Alejandro Sánchez Tadeo
;          A01377162 Guillermo Perez Trueba
;----------------------------------------------------------

;1. Or macro
(defmacro my-or
  "Evaluates its expressions one at a time, from left to right. If a form returns a logical true value,
  it returns that value and doesn't evaluate any of the other expressions, otherwise it returns the value of
  the last expression. (or) returns nil."
  ([] nil)
  ([x] x)
  ([x & expr]
   `(let [or# ~x]
      (if or#
        or#
        (my-or ~(first expr) ~@(rest expr))))))

;2 do-loop macro

(defmacro do-loop
  "Consists of a body (one or more expressions, presumably with side effects)
  and a final conditional form prefaced with a :while or :until keyword.
  First, the expressions in the body are evaluated sequentially, and
  then the condition is evaluated. Retuns nil"
  [& exprs]
  `(if (= :while (first (last(quote ~exprs))))
     (loop []
       (if (eval (second (last (quote ~exprs))))
         (do ~@(butlast exprs) (recur))))
     (loop []
       (if (not (eval (second (last (quote ~exprs)))))
         (do ~@(butlast exprs) (recur))))))


;3 def-pred
(defmacro def-pred
  "Define two predicate functions: a regular one;
   negated version (not- 'the same name') and its result negated (using the not function)."
  [name args & body]
  `(do
     (defn ~name ~args ~@body)
     (defn ~(symbol (str 'not- name)) ~args (not (do ~@body)))))


;4. Curry

(defn get_args
  "Getting parameters to form a function."
  [args body]
  (if (empty? args)
    `(do ~@body)
    `(fn ~(vector (first args))
       ~(get_args (rest args) body))))

(defmacro defn-curry
  "Performs a currying transformation to a function definition.
  It takes as parameters a name, an args vector, and a body of
  one or more expressions. The macro should define a function
  called name that takes only the first argument from args and
  returns a function that takes the second argument from args
  and returns a function that takes the third argument from args,
  and so on."
  [name args & body]
  (if (> (count args) 0)
    `(defn ~name
       [~(first args)]
       ~(get_args (rest args) body))
    `(defn ~name
       []
       (do ~@body))))

;5. If macro

(defn find-between
  [start end exprs]
  (->> exprs
       (drop-while #(not= start %)) ;find else
       rest
       (take-while #(not= end %)))) ;until end

(defmacro IF
  "Provide a conditional statement.Almost everything is optional in the IF macro, except condition.
  Also, the :ELSE keyword may come before the :THEN keyword."
  [condition & exprs]
  `(if ~condition
     (do ~@(find-between :THEN :ELSE exprs))
     (do ~@(find-between :ELSE :THEN exprs))))