;----------------------------------------------------------
; Activity: Problem Set: Recursive Functions I
; Date: August 26, 2018.
; Authors:
;          A01377515 Pablo Alejandro Sánchez Tadeo
;          A01377162 Guillermo Perez Trueba
;----------------------------------------------------------


(use 'clojure.test)



(defn my-count
  "Returns the number of elements contained in its input list."
  [a]
  (if (empty? a)
    0
    (+ 1 (my-count (rest a)))))

(deftest test-my-count
  (is (= 0 (my-count ())))
  (is (= 1 (my-count '(a))))
  (is (= 3 (my-count '(a b c)))))



(defn add-list
  "Returns the sum of all the elements of its input list, or 0 if its empty. Assume that all the elements
  in the input list are numbers."
  [a]
  (if (empty? a)
    0
    (+ (first a) (add-list (rest a)))))

(deftest test-add-list
  (is (= 0 (add-list ())))
  (is (= 10 (add-list '(2 4 1 3))))
  (is (= 55 (add-list '(1 2 3 4 5 6 7 8 9 10)))))



(defn member?
  "Verifies if x is a member of list lst."
  [x list]
  (if (= (first list) x)
    true
    (if (empty? list) false
      (member? x (rest list)))))

(deftest test-member?
  (is (not (member? 'a ())))
  (is (member? 'a '(a b c)))
  (is (member? 'a '(c b a b c)))
  (is (not (member? 'x '(a b c)))))



(defn list-of-symbols?
  "Returns true if all the elements (possibly zero) contained in lst are symbols, or false otherwise."
  [lst]
  (if (empty? lst)
    true
    (if (not (symbol? (first lst)))
      false
      (list-of-symbols? (rest lst)))))

(deftest test-list-of-symbols?
  (is (list-of-symbols? ()))
  (is (list-of-symbols? '(a)))
  (is (list-of-symbols? '(a b c d e)))
  (is (not (list-of-symbols? '(a b c d 42 e))))
  (is (not (list-of-symbols? '(42 a b c)))))



(defn my-last
  "It returns the last element of the given list, nil if the list is empty."
  [myList]
  (if (empty? myList)
    nil
    (if (= (my-count myList) 1)
      (first myList)
      (my-last (rest myList)))))

(deftest test-my-last
  (is (nil? (my-last ())))
  (is (= 'x (my-last '(x))))
  (is (= 'c (my-last '(a b c)))))



(defn cons-end
  "Returns a list composed by the same elements of lst but with x at the end."
  [x lst]
  (if (empty? lst)
    (cons x '())
    (cons (first lst) (cons-end x (rest lst)))))

(deftest test-cons-end
  (is (= '(b c d a) (cons-end 'a '(b c d))))
  (is (= '(a) (cons-end 'a ()))))


(defn my-reverse
  "Returns a list of the same elements of the input list but in reverse order."
  [myLst]
  (if (empty? myLst)
    []
    (conj (my-reverse (rest myLst)) (first myLst))))

(deftest test-my-reverse
  (is (= () (my-reverse ())))
  (is (= '(c b a) (my-reverse '(a b c))))
  (is (= '(3 (b c d) a) (my-reverse '(a (b c d) 3)))))


(defn my-butlast
  "Returns a list with the same elements as its input list but excluding the last element, or nil of its
  empty."
  [list]
  (cond
    (empty? list) nil
    (not (empty? (rest list))) (conj (my-butlast (rest list)) (first list))
    :else (conj '())))

(deftest test-my-butlast
  (is (nil? (my-butlast ())))
  (is (= () (my-butlast '(x))))
  (is (= '(a b) (my-butlast '(a b c)))))



(defn my-concat
  "Return the resulting list of appending two input lists."
  [list1 list2]
  (if (empty? list2)
    list1
    (my-concat (cons-end (first list2) list1) (rest list2))))

(deftest test-my-concat
  (is (= '(a b c) (my-concat '(a b c) ())))
  (is (= '(1 2 3) (my-concat () '(1 2 3))))
  (is (= '(a b c 1 2 3) (my-concat '(a b c) '(1 2 3)))))


(defn deep-reverse
  "Returns a list with the same elements as its input but in reverse order.
  Also reverses any nested lists."
  [list]
  (cond
    (empty? list) []
    (list? (first list)) (conj (deep-reverse (rest list))  (deep-reverse (first list)))
    :else (conj (deep-reverse (rest list)) (first list))))

(deftest test-deep-reverse
  (is (= () (deep-reverse ())))
  (is (= '(3 (d c b) a) (deep-reverse '(a (b c d) 3))))
  (is (= '(((6 5) 4) 3 (2 1)) (deep-reverse '((1 2) 3 (4 (5 6)))))))

(run-tests)