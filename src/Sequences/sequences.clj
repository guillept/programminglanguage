;----------------------------------------------------------
; Activity: Problem Set: Using the Sequence API
; Date: October 7, 2018.
; Authors:
;          A01377515 Pablo Alejandro Sanchez Tadeo
;          A01377162 Guillermo Perez Trueba
;----------------------------------------------------------

(use 'clojure.test)

;1
(defn positives
  "Takes a list of numbers lst as its argument, and returns a
  new list that only contains the positive numbers of lst"
  [lst]
  (filter pos? lst))

(deftest test-positives
  (is (= () (positives '())))
  (is (= () (positives '(-4 -1 -10 -13 -5))))
  (is (= '(3 6) (positives '(-4 3 -1 -10 -13 6 -5))))
  (is (= '(4 3 1 10 13 6 5) (positives '(4 3 1 10 13 6 5)))))


;2
(defn dot-product
  "It returns the result of performing the dot product of a times b."
  [a b]
  (reduce + (map * a b)))

(deftest test-dot-product
         (is (= 0 (dot-product () ())))
         (is (= 32 (dot-product '(1 2 3) '(4 5 6))))
         (is (= 21.45 (dot-product '(1.3 3.4 5.7 9.5 10.4)
                                   '(-4.5 3.0 1.5 0.9 0.0)))))

;3
(defn pow
  "Takes two arguments as input: a number a and a positive integer b.
  It returns the result of computing a raised to the power b."
  [a b]
  (reduce * (repeat b a)))


(deftest test-pow
  (is (= 1 (pow 0 0)))
  (is (= 0 (pow 0 1)))
  (is (= 1 (pow 5 0)))
  (is (= 5 (pow 5 1)))
  (is (= 125 (pow 5 3)))
  (is (= 25 (pow -5 2)))
  (is (= -125 (pow -5 3)))
  (is (= 1024 (pow 2 10)))
  (is (= 525.21875 (pow 3.5 5)))
  (is (= 129746337890625 (pow 15 12)))
  (is (= 3909821048582988049 (pow 7 22))))

;4
(defn replic
  "It returns a new list that replicates n times each element contained in lst"
  [n lst]
  (mapcat #(take n (repeat %)) lst))
;mapcat -> Returns the result of applying concat to the result of applying map to f and colls. Thus, function f should return a collection.

(deftest test-replic
         (is (= () (replic 7 ())))
         (is (= () (replic 0 '(a b c))))
         (is (= '(a a a) (replic 3 '(a))))
         (is (= '(1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4)
                (replic 4 '(1 2 3 4)))))

;5
(defn expand
  "Takes a list lst as its argument. It returns a list where the first element of
  lst appears one time, the second elements appears two times, the third element
  appears three times, and so on."
  [lst]
  (mapcat (fn [x n] (repeat x n))
          (range 1 (+ (count lst) 1))
          lst))

(deftest test-expand
  (is (= () (expand ())))
  (is (= '(a) (expand '(a))))
  (is (= '(1 2 2 3 3 3 4 4 4 4) (expand '(1 2 3 4))))
  (is (= '(a b b c c c d d d d e e e e e)
         (expand '(a b c d e)))))

;6
(defn largest
  "It returns the largest value contained in lst. Use the reduce function to solve this problem"
  [lst]
  ;(first (reverse (sort lst))) -> tambien funciona
  (reduce
    (fn [x i]
       (if (> i x)
         i
         x))

    (first lst) lst))

(deftest test-largest
         (is (= 31 (largest '(31))))
         (is (= 5 (largest '(1 2 3 4 5))))
         (is (= -1 (largest '(-1 -2 -3 -4 -5))))
         (is (= 52 (largest '(32 -1 45 12 -42 52 17 0 21 2)))))

;7
(defn drop-every
  "Takes two arguments: an integer number n, where n ≥ 1, and a list lst.
  It returns a new list that drops every n-th element from lst."
  [n lst]
  (mapcat rest
       (remove #(zero? (mod (first %) n))
               (map vector (range 1 (inc (count lst))) lst))))




(deftest test-drop-every
  (is (= () (drop-every 5 ())))
  (is (= '(1 2 3) (drop-every 4 '(1 2 3 4))))
  (is (= '(1 3 5 7) (drop-every 2 '(1 2 3 4 5 6 7 8))))
  (is (= '(1 3 5 7 9) (drop-every 2 '(1 2 3 4 5 6 7 8 9))))
  (is (= '(a b d e g h j)
         (drop-every 3 '(a b c d e f g h i j))))
  (is (= '(a b c d e f g h i j)
         (drop-every 20 '(a b c d e f g h i j))))
  (is (= () (drop-every 1 '(a b c d e f g h i j)))))

;8
(defn rotate-left
  "Returns the list that results from rotating lst a total of n elements to the left.
  If n is negative, it rotates to the right."
  [n lst]
  (if (= (count lst) 0)
    lst
    (if (>= (rem n 7) 0)
      (concat (second (split-at (rem n (count lst)) lst)) (first (split-at (rem n (count lst)) lst)))
      (concat (second (split-at (+ (count lst) (rem n (count lst))) lst)) (first (split-at  (+ (count lst) (rem n (count lst))) lst))))))

(deftest test-rotate-left
  (is (= () (rotate-left 5 ())))
  (is (= '(a b c d e f g) (rotate-left 0 '(a b c d e f g))))
  (is (= '(b c d e f g a) (rotate-left 1 '(a b c d e f g))))
  (is (= '(g a b c d e f) (rotate-left -1 '(a b c d e f g))))
  (is (= '(d e f g a b c) (rotate-left 3 '(a b c d e f g))))
  (is (= '(e f g a b c d) (rotate-left -3 '(a b c d e f g))))
  (is (= '(a b c d e f g) (rotate-left 7 '(a b c d e f g))))
  (is (= '(a b c d e f g) (rotate-left -7 '(a b c d e f g))))
  (is (= '(b c d e f g a) (rotate-left 8 '(a b c d e f g))))
  (is (= '(g a b c d e f) (rotate-left -8 '(a b c d e f g))))
  (is (= '(d e f g a b c) (rotate-left 45 '(a b c d e f g))))
  (is (= '(e f g a b c d) (rotate-left -45 '(a b c d e f g)))))

;9
(defn gcd
  "Takes two positive integer arguments a and b as arguments,
  where a > 0 and b > 0. It returns the greatest common
  divisor (GCD) of a and b."
  [a b]
  (last (remove nil? (map #(if (and (= (mod b %) 0) (= (mod a %) 0))
                             %)
                          (range 1 (+ (min a b) 1))))))

(deftest test-gcd
  (is (= 1 (gcd 13 7919)))
  (is (= 4 (gcd 20 16)))
  (is (= 6 (gcd 54 24)))
  (is (= 7 (gcd 6307 1995)))
  (is (= 12 (gcd 48 180)))
  (is (= 14 (gcd 42 56))))

;10
(defn insert-everywhere
  "It returns a new list with all the possible ways in which x can be inserted into every position of lst"
  [x lst]
  (for
    [i (range 0 (+ (count lst) 1))]
    (concat (take i lst) (cons x (drop i lst)))))
;take -> Returns a lazy sequence of the first n items in coll, or all items if there are fewer than n.
;drop -> Returns a lazy sequence of all but the first n items in coll.

(deftest test-insert-everywhere
    (is (= '((1)) (insert-everywhere 1 ())))
    (is (= '((1 a) (a 1)) (insert-everywhere 1 '(a))))
    (is (= '((1 a b c) (a 1 b c) (a b 1 c) (a b c 1))
           (insert-everywhere 1 '(a b c))))
    (is (= '((1 a b c d e)
             (a 1 b c d e)
             (a b 1 c d e)
             (a b c 1 d e)
             (a b c d 1 e)
             (a b c d e 1))
           (insert-everywhere 1 '(a b c d e))))
    (is (= '((x 1 2 3 4 5 6 7 8 9 10)
             (1 x 2 3 4 5 6 7 8 9 10)
             (1 2 x 3 4 5 6 7 8 9 10)
             (1 2 3 x 4 5 6 7 8 9 10)
             (1 2 3 4 x 5 6 7 8 9 10)
             (1 2 3 4 5 x 6 7 8 9 10)
             (1 2 3 4 5 6 x 7 8 9 10)
             (1 2 3 4 5 6 7 x 8 9 10)
             (1 2 3 4 5 6 7 8 x 9 10)
             (1 2 3 4 5 6 7 8 9 x 10)
             (1 2 3 4 5 6 7 8 9 10 x))
           (insert-everywhere 'x '(1 2 3 4 5 6 7 8 9 10)))))

(run-tests)